Django>=3.2
gunicorn>=20.1.0
python-decouple>=3.4
whitenoise>=5.2.0
