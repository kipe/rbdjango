# {{ project_name }}
#### Initialized with Reasonable Base for Django (RBDjango)
---

<!-- {% comment %} This content is removed once rendered with the template. -->
## To initialize a project
```sh
mkdir project && cd $_
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
pip install django
django-admin startproject --template https://gitlab.com/kipe/rbdjango/-/archive/master/rbdjango-master.zip --extension=py,yml,md,sh --name Dockerfile,.pylintrc PROJECT_NAME .
pip install -r requirements.txt -r requirements-dev.txt
```
<!-- {% endcomment %} -->

## To start the development container, run
```sh
docker-compose -f docker-compose.dev.yml up --build
```
The development container is self-reloading, happy hacking!

`makemigrations` and `migrate` are run every time the container is started, so if models are modified, restart the container.

# CI/CD
- Quality assurance, tests and Docker build are run on all branches
- The quality assurance
  - Includes checking the project with `flake8`, `pylint`, and `black`
  - Is allowed to fail, raising a warning if doing so
- Tests run four things
  - [Deployment check](https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/#run-manage-py-check-deploy)
    for deployment-safe settings
  - [Migrations check](https://docs.djangoproject.com/en/3.2/ref/django-admin/#makemigrations)
    ensuring that there's no pending migrations
  - [Static file collection](https://docs.djangoproject.com/en/3.2/ref/django-admin/#makemigrations)
    to make sure all files can be collected and other tasks, such as compressing the files can be done
  - Finally the tests are run using [the builtin testing of Django](https://docs.djangoproject.com/en/3.2/topics/testing/)
- Docker build is used to ensure the branch can be built

- When a tag matching `staging-*` or `release-*` is created
  - The previous steps (QA, tests, Docker build) are run
  - Docker build is tagged to `staging-latest` and `staging-YYMMDD` or `release-latest` and `release-YYMMDD`,
    depending on the tag
  - The stage is deployed using `./deploy.py` and [Portainer webhooks](https://documentation.portainer.io/v2.0/webhooks/create/)
    if webhooks are set to environment variable `PORTAINER_STAGING_WEBHOOKS` or `PORTAINER_PRODUCTION_WEBHOOKS`
  - The webhooks can be a comma-separated list of URLs to update multiple services at once
