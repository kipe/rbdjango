import os
import sys
import requests

# print(os.environ.get('PORTAINER_WEBHOOKS'))
PORTAINER_WEBHOOKS = list(filter(None, os.environ.get("PORTAINER_WEBHOOKS", "").split(",")))

if not PORTAINER_WEBHOOKS:
    print("PORTAINER_WEBHOOKS not set.")
    sys.exit(0)

fails = []
for hook in PORTAINER_WEBHOOKS:
    clean_hook = hook.split("//", 1)[1].split("/")[0]
    response = requests.post(hook)
    if response.status_code in [200, 204]:
        print(f"Deployment to {clean_hook} successful!")
    else:
        print(f"Deployment to {clean_hook} failed!")
        fails.append(1)

sys.exit(0 if not fails else 1)
