#!/bin/sh

until python3 manage.py initialize
do
  echo "Initializing..."
  sleep 5
done

# Run gunicorn with NUMBER_OF_CORES + 1
gunicorn --bind 0.0.0.0:8000 --workers $(($(nproc)+1)) {{ project_name }}.wsgi
