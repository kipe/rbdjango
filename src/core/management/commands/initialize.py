import logging
from decouple import config
from django.core import management
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


logger = logging.getLogger("initialize")


class Command(BaseCommand):
    help = "Initializes {{ project_name }}"

    def handle(self, *args, **options):
        management.call_command("migrate", interactive=False)
        if get_user_model().objects.filter(is_superuser=True).count() == 0:
            if config("DJANGO_SUPERUSER_USERNAME", default=None) and config("DJANGO_SUPERUSER_PASSWORD", default=None):
                management.call_command(
                    "createsuperuser",
                    interactive=False,
                    email=config("DJANGO_SUPERUSER_EMAIL", cast=str, default="{{ project_name }}@example.com"),
                )
            else:
                logger.info(
                    "Not creating superuser, as DJANGO_SUPERUSER_USERNAME "
                    "and DJANGO_SUPERUSER_PASSWORD haven't been set."
                )
        management.call_command("collectstatic", interactive=False)
