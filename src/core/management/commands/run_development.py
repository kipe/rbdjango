import logging
from django.core import management
from django.core.management.base import BaseCommand


logger = logging.getLogger("run_development")


class Command(BaseCommand):
    help = "Initializes and runs a development server"

    def handle(self, *args, **options):
        management.call_command("makemigrations")
        management.call_command("initialize")
        management.call_command("runserver", "0.0.0.0:8000")
