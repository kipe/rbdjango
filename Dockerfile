FROM python:3.9 AS base

ENV PYTHONUNBUFFERED=1
# Install requirements
ADD requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt \
    && rm /tmp/requirements.txt
# Copy project
WORKDIR /usr/src/{{ project_name }}

# development image
FROM base AS development

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
# Install development requirements
ADD requirements-dev.txt /tmp/requirements-dev.txt
RUN pip3 install -r /tmp/requirements-dev.txt \
    && rm /tmp/requirements-dev.txt
# Run the development server
CMD [ "python3", "manage.py", "run_development" ]

# Production image
FROM base AS production

ADD src/ .
# Run production server
CMD [ "./start.sh" ]
